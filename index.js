// Operators

// Assignment Operators
// Basic Assignment Operator (=)
let assignmentNumber = 8;

// Arithmetic Assignment Operators
// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;

console.log('Result of Addition Assignment Operator: ' + assignmentNumber);

assignmentNumber += 2;
console.log('Result of Addition Assignment Operator: ' + assignmentNumber);

// Subtraction Assignment Operator (-=)
assignmentNumber -= 2;

console.log('Result of Subtraction Assignment Operator: ' + assignmentNumber);

// Multiplication Assignment Operator (*=)
assignmentNumber *= 2;
console.log('Result of Multiplication Assignment Operator: ' + assignmentNumber);


// Division Assignment Operator (*=)
assignmentNumber /= 2;
console.log('Result of Division Assignment Operator: ' + assignmentNumber);

// Arithmetic Operators
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

difference = x - y;
console.log("Result of subtraction operator: " + difference);

product = x * y;
console.log("Result of multiplication operator: " + product);

quotient = x / y;
console.log("Result of division operator: " + quotient);

remainder = x % y;
console.log("Result of division operator: " + remainder);

// Multiple Operators and Parentheses

// Using Mdas
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parentheses
let pemdas = 1 + (2-3) * (4 / 5)
console.log("result of pemdas operation: " + pemdas)

// Increment (++) and Decrement (--)
let z = 1;

// Pre-increment
let increment = ++z;
console.log("Result of Pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Result of Post-increment: " + increment)

console.log("Result of Post-increment: " + z)


// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type coercion
console.log('type coercion')
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
// 1012
console.log(typeof coercion);
// string

let numC = true + 1;
// true = 1
console.log(numC)
console.log(typeof numC);

let numD = false + 1;
// false = 0
console.log(numD)
console.log(typeof numD);

// Relational operators
// Equality operator (==)
console.log('equality operator');

let juan = 'juan';
console.log(1 == 1);
// true
console.log(1 == 2);
// false
console.log(1 == '1');
// true
console.log('juan' == juan);
// true

// Inequality operator (!=)
console.log('inequality operator');
console.log('juan' != juan);
console.log(1 != 1);
// false
console.log(1 != 2);
// true
console.log(1 != '1');
// false
console.log('juan' != juan);
// false

// <,>,<=,>=
console.log('less than or greater than');
console.log(4 < 6);
// true
console.log(2 > 8);
// false
console.log(5 >= 5);
// true
console.log(10 <= 15);
// true

// Strict equality operator (===)
console.log('Strict Equality Operator');
console.log(1 === '1')
// false

// Strict inequality operator (!==)
console.log('Strict Inequality Operator')
console.log(1 !== '1')
// true

// Logical Operators
console.log('Logical Operators');
let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
console.log('And Operator');
// Return true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log('Result of logical AND operaator: ' + allRequirementsMet);
// Result of logical AND operaator: false

// OR Operator (||)
console.log('OR Operator');
// Return true if one of the operands is true
// Only False if both operands are false
let someRequerementsMet = isLegalAge || isRegistered;
console.log('Result of logical OR operator: ' + someRequerementsMet);
// Result of logical OR operator: true

// Selection Control Structures

// if, else if and else statement

// if statement
// Excecutes a statement if a specified condition is true

/* Syntax
	if(condition){
		statement/s;
	}
	else  if (condition1){
	statement/s;
	}
	else  if (condition2){
	statement/s;
	}
	else  if (condition3){
	statement/s;
	}
	else  if (condition4){
	statement/s;
	}
*/

let numE = -1;

if (numE < 0){
	console.log('Hello');
}

let nickName = 'Matt';

if(nickName === 'Mattheo'){
	console.log('Hello ' + nickName);
}

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true

let numF = 1;

if(numE > 0 ){
	console.log('hello');
}

else if(numF > 0){
	console.log('world');
}

// else clause
// executes a statement if all other conditions are not met

/* Syntax
	if(condition){
		statement/s;
	}
	else  if (condition1){
	statement/s;
	}
	else  if (condition2){
	statement/s;
	}
	else  if (condition3){
	statement/s;
	}
	else  if (conditionN){
	statement/s;
	}
	else{
		statement/s;
	}
*/

/*
	numE = -1;
	numF = 1;
*/
if(numE > 0 ){
	console.log('hello');
}
else if(numF === 0){
	console.log('world');
}
else{
		console.log('again');
}

let message = 'No message';
console.log(message);

function determineTyphoonintensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.';
	}
	else if(windSpeed <= 60){
		return 'Tropical Depression detected';
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical Storm detected';
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe Tropical Storm detected';
	}
	else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonintensity(68);
console.log(message);

// Ternary Operator
console.log('Ternary Operator');

/* Syntax:
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? true : false;
console.log('Result of Ternary Operator: ' + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// paresInt converts string values into numeric types
let age = parseInt(prompt('What is your age?'));
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name);